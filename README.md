### What is this repository for? ###

Simple Java project for simulating gravitational pull on bodies in closed system, it was created to explore Java's multithreading options.

More information can be found in [PDF file][0] (it's written in Lithuanian)

![Project in action](https://bytebucket.org/trodekas/orbits/raw/3b8ab0c00a5b90836d12e3d9572203e08d32bccf/ToUpload/Orbits.gif)

(GIF is 10FPS)

[0]: https://bitbucket.org/trodekas/orbits/raw/3b8ab0c00a5b90836d12e3d9572203e08d32bccf/ToUpload/LygiagretusIDSauliusStankeviciusIFF4-2.pdf