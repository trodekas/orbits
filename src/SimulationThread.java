import java.util.concurrent.CountDownLatch;

public class SimulationThread extends Thread
{
    private Thread t;
    private String threadName;

    private int firstBody;
    private int lastBody;

    private double totalForceX = 0; // Jėgų suma X atžvilgiu
    private double totalForceY = 0; // Jėgų suma Y atžvilgiu

    private CountDownLatch latch;

    SimulationThread(String name, int firstBody, int lastBody, CountDownLatch latch)
    {
        threadName = name;
        this.firstBody = firstBody;
        this. lastBody = lastBody;
        this.latch = latch;
    }

    public void run()
    {
        for(int i = firstBody; i < lastBody; i++)
        {
            this.simulateStep(i);
        }

        latch.countDown();
    }

    /** Simuliacijos žingsnis */
    private void simulateStep(int bodyId)
    {
        this.totalForceX = 0;
        this.totalForceY = 0;

        this.recalculateForcesAndAcceleration(Constants.G, bodyId);
        this.checkForConstraints(Constants.CONSTRAINT_UPPER_LEFT_X, Constants.CONSTRAINT_UPPER_LEFT_Y, Constants.CONSTRAINT_LOWER_RIGHT_X, Constants.CONSTRAINT_LOWER_RIGHT_Y, bodyId);
        this.recalculatePosition(SimulationManager.dTime, bodyId);
    }

    public void start ()
    {
        if (t == null)
        {
            t = new Thread (this, threadName);
            t.start ();
        }
    }

    /** Perskaičiuojama kūno pozicija X ašies atžvilgius */
    private void recalculatePositionX(double dTime, int bodyId)
    {
        /** Naujas pozicija */
        double newPositionX = SimulationManager.currentFrameBodies[bodyId].getPositionX() + SimulationManager.nextFrameBodies[bodyId].getVelocityX() * dTime + SimulationManager.nextFrameBodies[bodyId].getAccelerationX() * dTime * dTime / 2;
        /** Naujas greitis */
        double newVelocityX = SimulationManager.currentFrameBodies[bodyId].getVelocityX() + SimulationManager.nextFrameBodies[bodyId].getAccelerationX() * dTime;

        /** Įrašomi duomenys */
        SimulationManager.nextFrameBodies[bodyId].setPositionX(newPositionX);
        SimulationManager.nextFrameBodies[bodyId].setVelocityX(newVelocityX);
    }

    /** Perskaičiuojama kūno pozicija Y ašies atžvilgius */
    private void recalculatePositionY(double dTime, int bodyId)
    {
        /** Naujas pozicija */
        double newPositionY = SimulationManager.currentFrameBodies[bodyId].getPositionY() + SimulationManager.nextFrameBodies[bodyId].getVelocityY() * dTime + SimulationManager.nextFrameBodies[bodyId].getAccelerationY() * dTime * dTime / 2;
        /** Naujas greitis */
        double newVelocityY = SimulationManager.currentFrameBodies[bodyId].getVelocityY() + SimulationManager.nextFrameBodies[bodyId].getAccelerationY() * dTime;

        /** Įrašomi duomenys */
        SimulationManager.nextFrameBodies[bodyId].setPositionY(newPositionY);
        SimulationManager.nextFrameBodies[bodyId].setVelocityY(newVelocityY);
    }

    /** Perskaičiuojama kūno pozicija */
    public void recalculatePosition(double dTime, int bodyId)
    {
        this.recalculatePositionX(dTime, bodyId);
        this.recalculatePositionY(dTime, bodyId);
    }

    /** Perskaičiuojamas pagreitis X ašies atžvilgiu */
    private void recalculateAccelerationX(double forceX, int bodyId)
    {
        double newAccelerationX = forceX / SimulationManager.currentFrameBodies[bodyId].getMass();

        /** Įrašomi duomenys */
        SimulationManager.nextFrameBodies[bodyId].setAccelerationX(newAccelerationX);
    }

    /** Perskaičiuojamas pagreitis Y ašies atžvilgiu */
    private void recalculateAccelerationY(double forceY, int bodyId)
    {
        double newAccelerationY = forceY / SimulationManager.currentFrameBodies[bodyId].getMass();

        /** Įrašomi duomenys */
        SimulationManager.nextFrameBodies[bodyId].setAccelerationY(newAccelerationY);
    }

    /** Perskaičiuojamas pagreitis */
    private void recalculateAcceleration(double forceX, double forceY, int bodyId)
    {
        this.recalculateAccelerationX(forceX, bodyId);
        this.recalculateAccelerationY(forceY, bodyId);
    }

    /** Apskaičiuojama jėgų suma, kuri veikia kūną*/
    private void recalculateForces(double gConstant, int bodyId)
    {
        for(int otherBodyId = 0; otherBodyId < SimulationManager.currentFrameBodies.length; otherBodyId++)
        {
            if(bodyId != otherBodyId)
            {
                /** Apskaičiuojamas atstumas tarp kūnų X ašies atžvilgiu */
                double distanceX = SimulationManager.currentFrameBodies[otherBodyId].getPositionX() - SimulationManager.currentFrameBodies[bodyId].getPositionX();
                if(distanceX == 0) distanceX = Constants.EPS;
                /** Apskaičiuojamas atstumas tarp kūnų X ašies atžvilgiu */
                double distanceY = SimulationManager.currentFrameBodies[otherBodyId].getPositionY() - SimulationManager.currentFrameBodies[bodyId].getPositionY();
                if(distanceY == 0) distanceY = Constants.EPS;
                /** Apskaičiuojamas atstumas tarp kūnų */
                double distance = Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));

                /** Surandamas kūną veikiančios jėgos modulis */
                double force = recalculateForce(gConstant, otherBodyId, distance, bodyId);

                totalForceX += this.recalculateForceX(force, distance, distanceX);
                totalForceY += this.recalculateForceY(force, distance, distanceY);
            }
        }
    }

    /** Surandamas kūną veikiančios jėgos modulis pagal Niutono visuotinės traukos dėsnio formulę */
    private double recalculateForce(double gConstant, int otherBodyId, double distance, int bodyId)
    {
        return gConstant * SimulationManager.currentFrameBodies[bodyId].getMass() * SimulationManager.currentFrameBodies[otherBodyId].getMass() / distance;
    }

    /** Apskaičiuojama jėga veikianti kūna X ašies atžvilgiu */
    private double recalculateForceX(double force, double distance, double distanceX)
    {
        double cosAlfa = distanceX / distance;

        return cosAlfa * force;
    }

    /** Apskaičiuojama jėga veikianti kūna Y ašies atžvilgiu */
    private double recalculateForceY(double force, double distance, double distanceY)
    {
        double sinAlfa = distanceY / distance;

        return sinAlfa * force;
    }

    /** Apskaičiuojama jėga veikianti kūną ir perskaičiuojamas pagreitis */
    public void recalculateForcesAndAcceleration(double gConstant, int bodyId)
    {
        this.recalculateForces(gConstant, bodyId);
        this.recalculateAcceleration(totalForceX, totalForceY, bodyId);
    }

    /** Tikrinama ar kūnas atsitrenkė į kurią nors sieną */
    public void checkForConstraints(double constraintUpperLeftX, double constraintUpperLeftY, double constraintsLowerRightX, double constraintsLowerRightY, int bodyId)
    {
        this.upperConstraint(constraintUpperLeftY, bodyId);
        this.leftConstraint(constraintUpperLeftX, bodyId);
        this.lowerConstraint(constraintsLowerRightY, bodyId);
        this.rightConstraint(constraintsLowerRightX, bodyId);
    }

    /** Tikrinama ar kūnas atsitrenkė į viršutinę sieną */
    private void upperConstraint(double constraintUpperLeftY, int bodyId)
    {
        if(SimulationManager.currentFrameBodies[bodyId].getPositionY() <= constraintUpperLeftY)
        {
            /** Apskaičiuojamas atsitrenkimo poveikis kūnui */
            double newVelocityY = SimulationManager.currentFrameBodies[bodyId].getVelocityY() * -1 * Constants.STATIC_COLLISION_DAMPEN;
            /** Įrašomi duomenys */
            SimulationManager.nextFrameBodies[bodyId].setVelocityY(newVelocityY);
            SimulationManager.nextFrameBodies[bodyId].setPositionY(constraintUpperLeftY);
        }
    }

    /** Tikrinama ar kūnas atsitrenkė į apatinę sieną */
    private void lowerConstraint(double constraintLowerRightY, int bodyId)
    {
        if(SimulationManager.currentFrameBodies[bodyId].getPositionY() >= constraintLowerRightY)
        {
            /** Apskaičiuojamas atsitrenkimo poveikis kūnui */
            double newVelocityY = SimulationManager.currentFrameBodies[bodyId].getVelocityY() * -1 * Constants.STATIC_COLLISION_DAMPEN;
            /** Įrašomi duomenys */
            SimulationManager.nextFrameBodies[bodyId].setVelocityY(newVelocityY);
            SimulationManager.nextFrameBodies[bodyId].setPositionY(constraintLowerRightY);
        }
    }

    /** Tikrinama ar kūnas atsitrenkė į kairę sieną */
    private void leftConstraint(double constraintUpperLeftX, int bodyId)
    {
        if(SimulationManager.currentFrameBodies[bodyId].getPositionX() <= constraintUpperLeftX)
        {
            /** Apskaičiuojamas atsitrenkimo poveikis kūnui */
            double newVelocityX = SimulationManager.currentFrameBodies[bodyId].getVelocityX() * -1 * Constants.STATIC_COLLISION_DAMPEN;
            /** Įrašomi duomenys */
            SimulationManager.nextFrameBodies[bodyId].setVelocityX(newVelocityX);
            SimulationManager.nextFrameBodies[bodyId].setPositionX(constraintUpperLeftX);
        }
    }

    /** Tikrinama ar kūnas atsitrenkė į dešinę sieną */
    private void rightConstraint(double constraintLowerRightX, int bodyId)
    {
        if(SimulationManager.currentFrameBodies[bodyId].getPositionX() >= constraintLowerRightX)
        {
            /** Apskaičiuojamas atsitrenkimo poveikis kūnui */
            double newVelocityX = SimulationManager.currentFrameBodies[bodyId].getVelocityX() * -1 * Constants.STATIC_COLLISION_DAMPEN;
            /** Įrašomi duomenys */
            SimulationManager.nextFrameBodies[bodyId].setVelocityX(newVelocityX);
            SimulationManager.nextFrameBodies[bodyId].setPositionX(constraintLowerRightX);
        }
    }
}
