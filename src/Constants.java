/** Įvairios nekintačios konstantos */
public class Constants
{
    public final static double G = 0.00000005;

    public final static double CONSTRAINT_UPPER_LEFT_X = 0;
    public final static double CONSTRAINT_UPPER_LEFT_Y = 0;

    public final static double CONSTRAINT_LOWER_RIGHT_X = 800;
    public final static double CONSTRAINT_LOWER_RIGHT_Y = 600;

    public final static double EPS = Double.MIN_VALUE;

    public final static double STATIC_COLLISION_DAMPEN = 0.5;
}
