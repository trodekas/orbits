/** Duomenų struktūra skirta sauganti visus duomenis apie simuliuojamą kūna */
public class Body
{
    private double positionX; // Pozicija X ašies atžvilgiu
    private double positionY; // Pozicija Y ašies atžvilgiu

    private double radius;  // Kūno spindulys

    private double mass; // Kūno masė

    private double velocityX; // Greitis X ašies atžvilgiu
    private double velocityY; // Greitis Y ašies atžvilgiu

    private double accelerationX; // Pagreitus X ašies atžvilgiu
    private double accelerationY; // Pagreitis Y ašies atžvilgiu

    public Body(double positionX, double positionY, double radius, double mass, double velocityX, double velocityY, double accelerationX, double accelerationY)
    {
        this.positionX = positionX;
        this.positionY = positionY;

        this.radius = radius;

        this.mass = mass;

        this.velocityX = velocityX;
        this.velocityY = velocityY;

        this.accelerationX = accelerationX;
        this.accelerationY = accelerationY;
    }

    public double getPositionX()
    {
        return this.positionX;
    }

    public double getPositionY()
    {
        return this.positionY;
    }

    public double getRadius()
    {
        return this.radius;
    }

    public double getMass()
    {
        return this.mass;
    }

    public double getVelocityX()
    {
        return this.velocityX;
    }

    public double getVelocityY()
    {
        return this.velocityY;
    }

    public double getAccelerationX()
    {
        return this.accelerationX;
    }

    public double getAccelerationY()
    {
        return this.accelerationY;
    }

    public void setPositionX(double positionX)
    {
        this.positionX = positionX;
    }

    public void setPositionY(double positionY)
    {
        this.positionY = positionY;
    }

    public void setRadius(double radius)
    {
        this.radius = radius;
    }

    public void setMass(double mass)
    {
        this.mass = mass;
    }

    public void setVelocityX(double velocityX)
    {
        this.velocityX = velocityX;
    }

    public void setVelocityY(double velocityY)
    {
        this.velocityY = velocityY;
    }

    public void setAccelerationX(double accelerationX)
    {
        this.accelerationX = accelerationX;
    }

    public void setAccelerationY(double accelerationY)
    {
        this.accelerationY = accelerationY;
    }
}
