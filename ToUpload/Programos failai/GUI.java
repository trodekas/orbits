import javax.swing.*;
import java.awt.*;
import java.util.Locale;

/** Pagrindinė grafinės sąsajos klasė, bei programos pradžios taškas*/
public class GUI extends JFrame
{
    private Container container = new Container();

    public final static MainPanel mainPanel = new MainPanel();
    private final ControlPanel controlPanel;

    public GUI()
    {
        this.controlPanel = new ControlPanel();

        Locale.setDefault(Locale.US);

        setTitle("Orbits");
        setResizable(false);

        constructMainWindow();

        setVisible(true);
        pack();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void constructMainWindow()
    {
        container = getContentPane();

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        container.add(mainPanel);
        container.add(controlPanel);
    }

    public static void main(String[] args)
    {
        new GUI();
    }
}
