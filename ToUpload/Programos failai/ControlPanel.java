import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.concurrent.CountDownLatch;

/** Vartotojo sąsajos klasė skirta pradėti simuliaciją ir keisti simuliacijos nustatymus */
public class ControlPanel extends JPanel
{
    private static final JButton OK_BUTTON = new JButton("Pradėti");

    private static final JLabel THREAD_COUNT_LABEL = new JLabel("Gijos: ");
    private static final JLabel BODY_COUNT_LABEL = new JLabel("Kūnai: ");
    private static final JLabel USE_FIXED_TIME_STEP_LABEL = new JLabel("Fiksuotas laikas:");

    private static final JTextField THREADS_COUNT_FIELD = new JTextField("1", 4);
    private static final JTextField BODY_COUNT_FIELD = new JTextField("1600", 4);

    private static final JCheckBox USE_FIXED_TIME_STEP_CHECKBOX = new JCheckBox();

    private static SimulationManager simulationManager;
    private CountDownLatch latch;

    public ControlPanel()
    {
        constructPanel();
        addEventHandler();
    }

    private void constructPanel()
    {
        this.setBackground(Color.GRAY);

        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        this.add(USE_FIXED_TIME_STEP_LABEL);
        this.add(USE_FIXED_TIME_STEP_CHECKBOX);

        this.add(THREAD_COUNT_LABEL);
        this.add(THREADS_COUNT_FIELD);

        this.add(BODY_COUNT_LABEL);
        this.add(BODY_COUNT_FIELD);

        this.add(OK_BUTTON);
    }

    private void addEventHandler()
    {
        OK_BUTTON.addActionListener(this::okAction);
        USE_FIXED_TIME_STEP_CHECKBOX.addActionListener(this::setUseFixedTimeStepAction);
    }

    private void setUseFixedTimeStepAction(ActionEvent e)
    {
        simulationManager.setUseFixedTimeStep(USE_FIXED_TIME_STEP_CHECKBOX.isSelected());
    }

    private void okAction(ActionEvent e)
    {
        if(simulationManager != null)
        {
            System.out.println("Average time: " + simulationManager.getAverageCalculationTime());
            SimulationManager.go = false;
            try {
                latch.await();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            this.start();
        }
        else
        {
           this.start();
        }
    }

    private void start()
    {
        try
        {
            int bodyCount =  Integer.parseInt(BODY_COUNT_FIELD.getText());
            int threadCount = Integer.parseInt(THREADS_COUNT_FIELD.getText());

            if(bodyCount % threadCount == 0)
            {

                latch = new CountDownLatch(1);
                simulationManager = new SimulationManager(threadCount, bodyCount, latch, USE_FIXED_TIME_STEP_CHECKBOX.isSelected());
                simulationManager.start();
            }
            else
            {
                this.showInvalidNumbersError();
            }
        }
        catch (NumberFormatException e)
        {
            this.showInvalidFormatError();
        }
    }

    private void showInvalidFormatError()
    {
        JOptionPane.showMessageDialog(this, "Laukuose turi būti įvesti skaičiai", "Klaida!", JOptionPane.WARNING_MESSAGE);
    }

    private void showInvalidNumbersError()
    {
        JOptionPane.showMessageDialog(this, "Kūnų kiekis turėtų dalintis be liekanos iš gijų skaičiaus", "Klaida!", JOptionPane.WARNING_MESSAGE);
    }
}
