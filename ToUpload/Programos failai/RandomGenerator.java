import java.util.Random;

/** Klasė naudojama generuojant atsitinktinius kūnus */
public class RandomGenerator
{
    private static final double MIN_POS_X = 0;
    private static final double MAX_POS_X = 800;

    private static final double MIN_POS_Y = 0;
    private static final double MAX_POS_Y = 600;

    private static final double MIN_RADIUS = 1;
    private static final double MAX_RADIUS = 10;

    private static final double MIN_MASS = 1;
    private static final double MAX_MASS = 100;

    private static final double MIN_VEL_X = 0;
    private static final double MAX_VEL_X = 0;

    private static final double MIN_VEL_Y = 0;
    private static final double MAX_VEL_Y = 0;

    private static final double MIN_ACC_X = 0;
    private static final double MAX_ACC_X = 0;

    private static final double MIN_ACC_Y = 0;
    private static final double MAX_ACC_Y = 0;

    /** Grąžinamas atsitiktinai sugeneruotas kūnas */
    public static Body getRandomBody()
    {
        double positionX = getRandomDoubleInRange(MIN_POS_X, MAX_POS_X);
        double positionY = getRandomDoubleInRange(MIN_POS_Y, MAX_POS_Y);
        double radius = getRandomDoubleInRange(MIN_RADIUS, MAX_RADIUS);
        double mass = getRandomDoubleInRange(MIN_MASS, MAX_MASS);
        double velocityX = getRandomDoubleInRange(MIN_VEL_X, MAX_VEL_X);
        double velocityY = getRandomDoubleInRange(MIN_VEL_Y, MAX_VEL_Y);
        double accelerationX = getRandomDoubleInRange(MIN_ACC_X, MAX_ACC_X);
        double accelerationY = getRandomDoubleInRange(MIN_ACC_Y, MAX_ACC_Y);

        return new Body(positionX, positionY, radius, mass, velocityX, velocityY, accelerationX, accelerationY);
    }

    /** Grąžinama atsitiktinė reikšmė rėžiuose */
    public static double getRandomDoubleInRange(double min, double max)
    {
        return min + new Random().nextDouble() * (max - min);
    }
}
