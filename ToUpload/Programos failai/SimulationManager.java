import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

/** Klasė paveldanti iš Thread klasės skirta kitų gijų valdymui, bei pagrindinio simulaicijos ciklo vykdymo */
public class SimulationManager extends Thread
{
    private ArrayList<SimulationThread> simulationThreads = new ArrayList<>();

    public static Body currentFrameBodies[];
    public static Body nextFrameBodies[];

    public static long dTime; // Laiko tarpas

    private CountDownLatch latch;
    private int threadAmount; // Gijų skaičius
    private int bodyAmount; // Kūnų kiekis
    private int bodyCountForThread; // Kūnų skaičius skirtas kiekvienai gijai

    public static boolean go = false; // Ar vykdyti pagrindinį ciklą

    private Thread t;
    private CountDownLatch myLatch;

    private boolean useFixedTimeStep = false; // Ar naudoti fiksuotą laiko žingsnį
    private int iteration = 0; // Įvykdytų iteracijų skaičius
    private long calculationTime = 0; // Kintamas saugantis pilną skaičiavimų trukmę

    public SimulationManager(int threadAmount, int bodyAmount, CountDownLatch latch, boolean useFixedTimeStep)
    {
        this.threadAmount = threadAmount;
        this.bodyAmount = bodyAmount;
        this.bodyCountForThread = bodyAmount / threadAmount;
        this.useFixedTimeStep = useFixedTimeStep;

        this.myLatch = latch;
    }

    public void start ()
    {
    if (t == null)
    {
        t = new Thread (this, "Manager");
        t.start ();
    }
}

    /** Sukuriamas skaičiavimų gijos ir priskiriami duomenys */
    private void initThreads(int threadAmount)
    {
        simulationThreads.clear();

        for(int i = 0; i < threadAmount; i++)
        {
            simulationThreads.add(new SimulationThread("Thread " + i, i * bodyCountForThread, (i + 1) * bodyCountForThread, latch));
        }
    }

    private void startThreads()
    {
        for(int i = 0; i < simulationThreads.size(); i++)
        {
            simulationThreads.get(i).start();
        }
    }

    /** Sugeneruojama nurodytas kiekis atsitiktinių kūnų */
    private void generateRandomBodies(int amount)
    {
        currentFrameBodies = new Body[amount];
        nextFrameBodies = new Body[amount];

        for(int i = 0; i < amount; i++)
        {
            Body randomBody = RandomGenerator.getRandomBody();
            currentFrameBodies[i] = randomBody;
            nextFrameBodies[i] = randomBody;
        }
    }

    /** Perkopijuojami sekančio kadro duomenys į esamo kadro masyvą */
    private void copyNextToCurrent()
    {
        System.arraycopy(currentFrameBodies, 0, nextFrameBodies, 0, nextFrameBodies.length);
    }

    /** Pagrindinis simuliacijos ciklas */
    public void mainLoop()
    {
        long prevTime = System.currentTimeMillis();
        this.calculationTime = 0; // Kintamas saugantis pilną skaičiavimų trukmę
        this.iteration = 0; // Įvykdytų iteracijų skaičius

        while(go)
        {
            iteration++;
            long currentTime = System.currentTimeMillis();
            /** Nustatomas simuliacijos žingsnio laiko tarpas */
            if(!useFixedTimeStep)
            {
                dTime = currentTime - prevTime;
            }
            else
            {
                dTime = 16;
            }

            //long startInit = System.currentTimeMillis();
            /** Sukuriamos ir statuojamos skačiavimų gijos */
            latch = new CountDownLatch(threadAmount);
            this.initThreads(threadAmount);
            this.startThreads();
            //long dInit = System.currentTimeMillis() - startInit;
            //System.out.println("Init: " + dInit);

            /** Laukiama, kol visos skaičiavimų gijos baigs savo darbą */
            long startWait = System.currentTimeMillis();
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long dWait = System.currentTimeMillis() - startWait;
            //System.out.println(dWait);
            this.calculationTime += dWait;

            /** Perpaišomi rezultatai */
            //long startRepaint = System.currentTimeMillis();
            GUI.mainPanel.repaint();
            //long dRepaint = System.currentTimeMillis() - startRepaint;
            //System.out.println("        Repaint: " + dRepaint);

            /** Perkopijuojami duomenys */
            //long startCopy = System.currentTimeMillis();
            this.copyNextToCurrent();
            //long dCopy = System.currentTimeMillis() - startCopy;
            //System.out.println("            Copy: " + dCopy);

            prevTime = currentTime;
        }

        myLatch.countDown();
    }

    public void run()
    {
        this.generateRandomBodies(bodyAmount);

        go = true;
        this.mainLoop();
    }

    /** Nustatoma ar naudoti fiksuotą laiko žingsį */
    public synchronized void setUseFixedTimeStep(boolean use)
    {
        this.useFixedTimeStep = use;
    }

    /** Grąžinamas vidutinė vieno kadro skaičiavimų trukmė */
    public synchronized double getAverageCalculationTime()
    {
        return calculationTime / iteration;
    }
}
