import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

/** Klasė skirta vizualizuoti rezultatus */
public class MainPanel extends JPanel
{
    private static final int PREFERRED_WIDTH = 800;
    private static final int PREFERRED_HEIGHT = 600;

    private static final Color BODY_COLOR = Color.white;
    private static final Color BACKGROUND_COLOR = new Color(5, 5, 38);

    private static final double BODY_RADIUS = 5;

    public MainPanel()
    {
        this.setBackground(BACKGROUND_COLOR);
    }

    /** Metodas nupaišantis kūnus grafinėje sąsajoje */
    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(BODY_COLOR);

        if(SimulationManager.go) {
            for (int i = 0; i < SimulationManager.nextFrameBodies.length; i++) {
                Ellipse2D.Double body = new Ellipse2D.Double(SimulationManager.nextFrameBodies[i].getPositionX() - BODY_RADIUS,
                        SimulationManager.nextFrameBodies[i].getPositionY() - BODY_RADIUS,
                        BODY_RADIUS, BODY_RADIUS);

                g2.fill(body);
            }
        }

    }

    @Override
    public Dimension getPreferredSize()
    {
        return new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT);
    }
}